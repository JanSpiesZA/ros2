//Boilerplate code for robot used in IIS301T, MRO216D

//{===================================================}
//Do not change the following
//Standard includes used by microros and ros
#include <micro_ros_arduino.h>

#include <stdio.h>
#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>

//Standard instantiantions used by microros
rclc_executor_t executor;
rclc_support_t support;
rcl_allocator_t allocator;
rcl_node_t node;
rcl_timer_t timer;

#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){error_loop();}}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){}}
//{===================================================}


//The following area can be edited and added by the user
//Include the message types used by the publishers and subscribers in this area
#include "arduino_secrets.h"
#include <std_msgs/msg/int8.h>


//Define subscribers in this area
rcl_subscription_t LEDs_subscriber;


//Creat variables used by the publishers and the subscribers
std_msgs__msg__Int8 LEDs_msg;


//Define pins to be used on the hardware
#define LEFT_LED_PIN 17
#define RIGHT_LED_PIN 16


void error_loop(){
  while(1){
    digitalWrite(LEFT_LED_PIN, !digitalRead(LEFT_LED_PIN));
    delay(100);
  }
}

void LEDs_subscription_callback(const void * msgin)
{  
  const std_msgs__msg__Int8 * msg = (const std_msgs__msg__Int8 *)msgin;
  
  int8_t value = msg->data;
  
  switch (value) {
    case 0:
      digitalWrite(LEFT_LED_PIN, LOW);
      digitalWrite(RIGHT_LED_PIN, LOW);
      break;
    case 1:
      digitalWrite(LEFT_LED_PIN, HIGH);
      digitalWrite(RIGHT_LED_PIN, LOW);
      break;
    case 2:
      digitalWrite(LEFT_LED_PIN, LOW);
      digitalWrite(RIGHT_LED_PIN, HIGH);
      break;
    case 3:
      digitalWrite(LEFT_LED_PIN, HIGH);
      digitalWrite(RIGHT_LED_PIN, HIGH);
      break;
    default:
      break;
  }  
}

void setup() 
{
  //## Use this method if the device is connected to the host via USB
  //set_microros_transports();
  
  //## Use this method if the device is connected to the host via WiFi
  set_microros_wifi_transports(SECRET_SSID, SECRET_PASS, "192.168.1.132", 8989);  
    
  //## Setup IO pins
  pinMode(LEFT_LED_PIN, OUTPUT);
  digitalWrite(LEFT_LED_PIN, HIGH);  
  pinMode(RIGHT_LED_PIN, OUTPUT);
  digitalWrite(RIGHT_LED_PIN, HIGH);  
  
  delay(2000);

  allocator = rcl_get_default_allocator();

  //create init_options
  RCCHECK(rclc_support_init(&support, 0, NULL, &allocator));

  // create node
  RCCHECK(rclc_node_init_default(&node, "esp32_CSE_robot_node", "", &support));

  // create subscriber
  RCCHECK(rclc_subscription_init_default(
    &LEDs_subscriber,
    &node,
    ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Int8),
    "LEDs_subscriber"));

  // create executor
  RCCHECK(rclc_executor_init(&executor, &support.context, 1, &allocator));
  RCCHECK(rclc_executor_add_subscription(&executor, &LEDs_subscriber, &LEDs_msg, &LEDs_subscription_callback, ON_NEW_DATA));
}

void loop() {
  delay(100);
  RCCHECK(rclc_executor_spin_some(&executor, RCL_MS_TO_NS(100)));
}
